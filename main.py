import random
import sys

if len(sys.argv) < 2:
    print('Please specify the filename')
    exit()

filename = sys.argv[1]

with open(filename) as input_file:
    lines = [line.strip() for line in input_file.read().split('\n')]
    configuration = list(map(int, lines[0].split()))
    baskets = []
    current_basket = []
    for line in lines[1:]:
        if line == '#':
            baskets.append(current_basket)
            current_basket = []
        else:
            current_basket.append(line)

if len(baskets) != len(configuration):
    print('Invalid configuration')
    exit()

n_groups = max(len(basket) // config for basket, config in zip(baskets, configuration))

for basket in baskets:
    random.shuffle(basket)

groups = []
for group_id in range(n_groups):
    current_group = []
    for basket in baskets:
        current_group.extend(basket[group_id::n_groups])
    groups.append(current_group)

for id, group in enumerate(groups):
    group_letter = chr(65 + id)
    print(f'Group {group_letter}:')
    for player in group:
        print(player)
    print()
